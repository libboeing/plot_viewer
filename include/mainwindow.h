/**
    Plot Viewer project
    mainwindow.h
*/

#pragma once

#include <QMainWindow>
#include <QPixmap>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void slotFileOpenRequest();
    void slotErrorMessage(const QString &);
    void slotSetBusy(bool);
    void slotHeaderChanged(const QString &);

signals:
    void fileSelected(const QString &filename);

private:
    Ui::MainWindow *ui;
    QPixmap icon_busy { };
    QPixmap icon_idle { };
};
