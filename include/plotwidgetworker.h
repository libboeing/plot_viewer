/**
    Plot Viewer project
    plotwidgetworker.h
*/

#pragma once

#include <memory>
#include <mutex>
#include <QtCore>
#include <QPixmap>
#include <QRect>
#include <QThread>
#include "plotwidget.h"
#include "viewport.h"

using Point = std::pair<double, double>;

class PlotWidget::PlotWidgetWorker : public QThread
{
    Q_OBJECT

public:
    std::unique_ptr<QPixmap> plot_pixmap { };
    std::mutex plot_pixmap_mutex { };

    PlotWidgetWorker(PlotWidget &plotWidget, QWidget *parent = nullptr);
    virtual ~PlotWidgetWorker() { }
    void run();
    QRect plotRect() { return m_plot_rect; }
    std::pair<Point, Point> visibleRect() { return m_viewport.visibleRect(); }
    void setMarkerPosition(int);
    bool isGenerationActive() { return m_generation_active; }

public slots:
    void slotLoadPoints(const QString &filename);
    void slotGeneratePlotRender();
    void slotUpdatePlotRect(QRect);
    void slotUpdatePlotRectOnly(QRect);

    void slotZoomIn();
    void slotZoomOut();
    void slotUnzoom();
    void slotMoveLeft();
    void slotMoveRight();

signals:
    void errorOccurred(const QString &message);
    void busyChanged(bool);
    void generatedPlotRender();
    void headerChanged(const QString &);

private:
    PlotWidget &plotWidget;
    QRect m_plot_rect { };
    PlotModel::Viewport m_viewport;
    int m_marker_position { };
    bool m_generation_active { };

    double translateMarkerPosition();
};
