/**
    Plot Viewer project
    plotwidget.h
*/

#pragma once

#include <QtCore>
#include <QRect>
#include <QWidget>

class PlotWidget : public QWidget
{
    Q_OBJECT

public:
    PlotWidget(QWidget *parent = nullptr);
    virtual QSize minimumSizeHint() const;

signals:
    void requestedLoadPoints(const QString &filename);
    void requestedUpdatePlotRect(QRect);
    void requestedUpdatePlotRectOnly(QRect);
    void requestedGeneratePlotRender();
    void busyChanged(bool);
    void errorOccurred(const QString &message);
    void requstedZoomIn();
    void requstedZoomOut();
    void requstedUnzoom();
    void requstedMoveLeft();
    void requstedMoveRight();
    void headerChanged(const QString &);

protected:
    virtual void paintEvent(QPaintEvent *);
    virtual void resizeEvent(QResizeEvent *);
    virtual void mousePressEvent(QMouseEvent *);

private:
    static const int m_left_panel_width { 110 };
    static const int m_bottom_panel_height { 30 };
    static const int m_margin { 10 };

    class PlotWidgetWorker;
    PlotWidgetWorker *m_worker;
    bool new_plot_render_available { };
    int marker_position { -1 };

private slots:
    void slotDrawPlot();
};
