/**
    Plot Viewer project
    pointsetreader.h
*/

#pragma once

#include <iterator>
#include <vector>
#include <utility>

using Point = std::pair<double, double>;
using Container = std::vector<Point>;
using Size = std::pair<int, int>;
using ScreenPoint = std::pair<double, double>;

namespace PlotModel {

// Viewport is used as toolkit-independent plot representation helper class
class Viewport
{
public:
    explicit Viewport();

    Size size() const;
    void setSize(Size);
    const Container &points() const;
    void setPoints(Container &); // It is assumed that container is sorted by X values
    void setPoints(Container &&);

    void zoomIn(double zoom_center);
    void zoomOut(double zoom_center);
    void restoreFullScale();
    void moveLeft();
    void moveRight();
    std::pair<Point, Point> visibleRect();

    class PixelIterator
    {
    public:
        explicit PixelIterator(Viewport &);
        class Sentinel { };
        ScreenPoint operator*();
        PixelIterator &operator++();
        bool operator!=(const Sentinel &) const;
        Sentinel end() { return Sentinel { }; }

    private:
        const Viewport &viewport;
        size_t point_index { };
        ScreenPoint last_screen_point { };
        ScreenPoint convertPointToPixelPos();
    };
    PixelIterator begin() { return PixelIterator(*this); }
    PixelIterator::Sentinel end() { return PixelIterator::Sentinel(); }

private:
    int m_pixel_width { };
    int m_pixel_height { };

    // Axes boundaries for all points
    size_t m_min_y_index_total { };
    size_t m_max_y_index_total { };

    // Axes boundaries for points included in current zoomed area
    double m_min_x_zoom { };
    double m_max_x_zoom { };
    size_t m_min_y_index_zoom { };
    size_t m_max_y_index_zoom { };

    Container m_points { };

    void resetAll();
    size_t leftPointIndex() const;
    size_t rightPointIndex() const;
    void updateYMinMax();
};

}

namespace std {
template <>
struct iterator_traits<PlotModel::Viewport::PixelIterator> {
    using iterator_category = std::forward_iterator_tag;
    using value_type = ScreenPoint;
};
}
