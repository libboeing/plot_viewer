/**
    Plot Viewer project
    plotexception.h
*/

#pragma once

#include <stdexcept>
#include <string>
#include <string_view>

namespace PlotModel {

class PlotException : public std::runtime_error
{
public:
    PlotException(const std::string &what_arg) : std::runtime_error(what_arg) { }
    PlotException(const std::string_view &what_arg) : std::runtime_error(std::string { what_arg }) { }
    PlotException(const char what_arg[]) : std::runtime_error(what_arg) { }
};

}
