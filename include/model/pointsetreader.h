/**
    Plot Viewer project
    pointsetreader.h
*/

#pragma once

#include <filesystem>
#include <fstream>
#include <iterator>
#include <limits>
#include <stddef.h>
#include <string>
#include <vector>
#include <utility>

using Point = std::pair<double, double>;
static const auto point_pos_min = std::numeric_limits<Point::first_type>::lowest();

namespace PlotModel {

// PointsetReader is used to read point dataset & metainfo from a file
class PointsetReader
{
public:
    explicit PointsetReader(const std::filesystem::path &filename);
    PointsetReader(const PointsetReader &) = delete;
    PointsetReader(PointsetReader &&);
    PointsetReader &operator=(const PointsetReader &) = delete;
    PointsetReader &operator=(PointsetReader &&);
    virtual ~PointsetReader() { }

    std::string header() { return m_header; }

    // Internal iterator for points
    class Sentinel { };
    class Iterator
    {
    public:
        Iterator(PointsetReader &reader);
        Point operator*() const;
        Iterator &operator++();
        bool operator!=(const Sentinel &) const;
    private:
        PointsetReader &m_parent;
    };
    Iterator begin() { return Iterator(*this); }
    Sentinel end() { return Sentinel { }; }

    size_t estimated_size() const { return m_estimated_size; }

private:
    size_t m_estimated_size { };
    bool m_end_reached { };
    std::ifstream m_stream;
    std::string m_current_line { };
    Point m_current_point { point_pos_min, { } };
    std::string m_header { };

    void parseCurrentPoint();
    void finishParsing();
};

}

namespace std {
template <>
struct iterator_traits<PlotModel::PointsetReader::Iterator> {
    using iterator_category = std::input_iterator_tag;
    using value_type = Point;
};
}
