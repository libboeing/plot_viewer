# Purpose

**plot_viewer** is a plot displaying application for files created in TimeView32.

# File format

Application is able to load measurement data from files of the following format:

    # Pendulum Instruments AB, TimeView32 V1.01      // Organization name, name and version of application this file was created by
    # FREQUENCY A                                    // Measurement type
    # Thu Nov 27 12:26:21 2003                       // Measurement start time
    # Measuring time: 800 ns  Single: Off            // Measurement parameters
    # Input A: Auto, 1Mк, AC, X1, Pos  Filter: Off   // Measurement parameters
    # Input B: Auto, 1Mк, DC, X1, Pos  Common: On    // Measurement parameters
    # Ext.arm: Off   Ref.osc: Internal               // Measurement parameters
    # Hold off: Off    Statistics: Off               // Measurement parameters
    0.0000000000000e+000 1.6394073114428e+004            
    3.0724070450092e-003 1.6395409053595e+004
    6.1448140900184e-003 1.6379824408800e+004

All header lines begin with #. Spacing between fields inside the header lines is not fixed and can be filled by spaces or tabs. Measurement data is represented by <timestamp> <value> pairs of numbers. Timestamps are sorted using ascending order, <value> can have any value. 

# Dependencies

* Qt 5 (tested with 5.15.2)
* Boost (tested with 1.75)

# Operating system

Currenly **plot_viewer** is able to run in Linux.
