/**
    Plot Viewer project
    plotwidgetworker.h
*/

#include <memory>
#include <optional>
#include <vector>
#include <utility>
#include <QtCore>
#include <QBrush>
#include <QPainter>
#include <QPaintEvent>
#include <QPen>
#include <QPixmap>
#include <QRect>
#include <QSize>
#include "plotexception.h"
#include "pointsetreader.h"
#include "plotwidget.h"
#include "plotwidgetworker.h"

PlotWidget::PlotWidgetWorker::PlotWidgetWorker(PlotWidget &plotWidget, QWidget *parent) :
    QThread(parent),
    plotWidget(plotWidget)
{

}

void PlotWidget::PlotWidgetWorker::run()
{
    try {
        exec();
    } catch (...) {
        qWarning() << "PlotWidgetWorker crashed during message loop";
    }
}

void PlotWidget::PlotWidgetWorker::setMarkerPosition(int position)
{
    m_marker_position = position;
}

void PlotWidget::PlotWidgetWorker::slotLoadPoints(const QString &filename)
{
    emit busyChanged(true);
    auto points { std::vector<std::pair<double, double>>() };
    try {
        auto reader { PlotModel::PointsetReader(filename.toStdU16String()) };
        //emit headerChanged(QString::fromStdString(reader.header()));
        emit headerChanged(QString::fromUtf8(reader.header().c_str()));

        try {
            points.reserve(reader.estimated_size());
        } catch (const std::bad_alloc &) {
            emit errorOccurred("Could not allocate memory for points");
            emit busyChanged(false);
            return;
        }

        for (auto point : reader)
            points.push_back(point);
    } catch (const std::exception &e) {
        emit errorOccurred(e.what());
    }
    m_viewport.setPoints(std::move(points));
    plotWidget.new_plot_render_available = false;
    plotWidget.update();
    emit busyChanged(false);
}

void PlotWidget::PlotWidgetWorker::slotGeneratePlotRender()
{
    emit busyChanged(true);
    m_generation_active = true;
    auto _ { std::lock_guard(plot_pixmap_mutex) };
    plot_pixmap = std::make_unique<QPixmap>(m_plot_rect.size());
    auto painter { QPainter { plot_pixmap.get() } };
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setPen(QPen {Qt::NoPen});
    painter.setBrush(QBrush {Qt::white});
    painter.drawRect(0, 0, m_plot_rect.width(), m_plot_rect.height());
    painter.setPen(QPen {Qt::blue, 1});
    painter.setBrush(QBrush {Qt::blue});

    auto drawEllipse { [&painter] (const auto & point)
    {
        painter.drawEllipse(QPointF{point.first, point.second}, 2.0, 2.0);
    } };
    std::optional<std::pair<double, double>> last_point { };
    for (auto point : m_viewport) {
        if (!last_point) {
            last_point = point;
            drawEllipse(point);
            continue;
        }
        painter.drawLine(
            QPointF { last_point->first, last_point->second },
            QPointF { point.first, point.second } );
        drawEllipse(point);
        last_point = point;
    }
    plotWidget.new_plot_render_available = true;
    m_generation_active = false;
    emit generatedPlotRender();
    emit busyChanged(false);
}

void PlotWidget::PlotWidgetWorker::slotUpdatePlotRect(QRect r)
{
    slotUpdatePlotRectOnly(r);
    slotGeneratePlotRender();
}

void PlotWidget::PlotWidgetWorker::slotUpdatePlotRectOnly(QRect r)
{
    m_plot_rect = r;
    m_viewport.setSize({m_plot_rect.width(), m_plot_rect.height()});
}

void PlotWidget::PlotWidgetWorker::slotZoomIn()
{
    auto pos = translateMarkerPosition();
    m_viewport.zoomIn(pos);
    slotGeneratePlotRender();
}

void PlotWidget::PlotWidgetWorker::slotZoomOut()
{
    auto pos = translateMarkerPosition();
    m_viewport.zoomOut(pos);
    slotGeneratePlotRender();
}

void PlotWidget::PlotWidgetWorker::slotUnzoom()
{
    m_viewport.restoreFullScale();
    slotGeneratePlotRender();
}

void PlotWidget::PlotWidgetWorker::slotMoveLeft()
{
    m_viewport.moveLeft();
    slotGeneratePlotRender();
}

void PlotWidget::PlotWidgetWorker::slotMoveRight()
{
    m_viewport.moveRight();
    slotGeneratePlotRender();
}

double PlotWidget::PlotWidgetWorker::translateMarkerPosition()
{
    auto rect = m_viewport.visibleRect();
    auto width = rect.second.first - rect.first.first;
    if (width == 0.0 || m_plot_rect.width() == 0)
        return { };
    return rect.first.first + (static_cast<double>(m_marker_position)) / m_plot_rect.width() * width;
}
