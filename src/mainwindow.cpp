/**
    Plot Viewer project
    mainwindow.cpp
*/

#include <QFileDialog>
#include <QMessageBox>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setStatusBar(nullptr);
    {
        icon_busy = QPixmap(":/hourglass.png");
        icon_idle = QPixmap(":/hourglass_inactive.png");
        ui->label_2->setPixmap(icon_idle);
    }
    connect(this, SIGNAL(fileSelected(QString)), ui->widget, SIGNAL(requestedLoadPoints(QString)));
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(slotFileOpenRequest()));
    connect(ui->widget, SIGNAL(errorOccurred(QString)), this, SLOT(slotErrorMessage(QString)));
    connect(ui->widget, SIGNAL(busyChanged(bool)), this, SLOT(slotSetBusy(bool)));
    connect(ui->widget, SIGNAL(headerChanged(QString)), this, SLOT(slotHeaderChanged(QString)));

    connect(ui->pushButton_2, SIGNAL(clicked()), ui->widget, SIGNAL(requstedZoomIn()));
    connect(ui->pushButton_3, SIGNAL(clicked()), ui->widget, SIGNAL(requstedZoomOut()));
    connect(ui->pushButton_4, SIGNAL(clicked()), ui->widget, SIGNAL(requstedUnzoom()));
    connect(ui->pushButton_5, SIGNAL(clicked()), ui->widget, SIGNAL(requstedMoveLeft()));
    connect(ui->pushButton_6, SIGNAL(clicked()), ui->widget, SIGNAL(requstedMoveRight()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::slotFileOpenRequest()
{
    auto filename { QFileDialog::getOpenFileName(nullptr,
                                                 "Select file with points data",
                                                 {}, "*")};
    if (!filename.isEmpty())
        emit fileSelected(filename);
}

void MainWindow::slotErrorMessage(const QString &msg)
{
    QMessageBox::critical(this, "Error", msg);
}

void MainWindow::slotSetBusy(bool busy)
{
    ui->label_2->setPixmap(busy ? icon_busy : icon_idle);
}

void MainWindow::slotHeaderChanged(const QString &text)
{
    ui->textEdit->setText(text);
}

