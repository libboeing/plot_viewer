/**
    Plot Viewer project
    viewport.cpp
*/


#include <algorithm>
#include <iterator>
#include <stdexcept>
#include "plotexception.h"
#include "viewport.h"

using Point = std::pair<double, double>;
using Container = std::vector<Point>;
using Size = std::pair<int, int>;
using ScreenPoint = std::pair<double, double>;

static const double zoom_factor = 1.2;

static auto minmaxElementByY(Container::iterator begin, Container::iterator end)
{
    return std::minmax_element(begin, end, [] (auto a, auto b) {
        return a.second < b.second;
    });
}

namespace PlotModel {

Viewport::Viewport()
{

}

Size Viewport::size() const
{
    return { m_pixel_width, m_pixel_height };
}

void Viewport::setSize(Size size)
{
    if (size.first < 0 || size.second < 0)
        throw PlotException("Wrong viewport dimensions");
    m_pixel_width = size.first;
    m_pixel_height = size.second;
}

const Container &Viewport::points() const
{
    return m_points;
}

void Viewport::setPoints(Container &points)
{
    m_points = points;
    resetAll();
}

void Viewport::setPoints(Container &&points)
{
    m_points = std::move(points);
    resetAll();
}

void Viewport::zoomIn(double zoom_center)
{
    if (m_points.size() == 0)
        return;
    zoom_center = std::clamp(zoom_center, m_min_x_zoom, m_max_x_zoom);
    auto distance_left { (zoom_center - m_min_x_zoom) / zoom_factor };
    auto distance_right { (m_max_x_zoom - zoom_center) / zoom_factor };
    m_min_x_zoom = zoom_center - distance_left;
    m_max_x_zoom = zoom_center + distance_right;

    // Check if Y min/max are still in zoomed area, update min/max otherwise
    auto left_index { leftPointIndex() };
    auto right_index { rightPointIndex() };
    if (!(m_min_y_index_zoom >= left_index && m_min_y_index_zoom <= right_index &&
            m_max_y_index_zoom >= left_index && m_max_y_index_zoom <= right_index)) {
        auto [min_el, max_el] { minmaxElementByY(m_points.begin() + left_index, m_points.begin() + right_index + 1) };
        m_min_y_index_zoom = std::distance(m_points.begin(), min_el);
        m_max_y_index_zoom = std::distance(m_points.begin(), max_el);

    }
}

void Viewport::updateYMinMax()
{
    auto left_index { leftPointIndex() };
    auto right_index { rightPointIndex() };
    auto [min_el, max_el] { minmaxElementByY(m_points.begin() + left_index, m_points.begin() + right_index + 1) };
    m_min_y_index_zoom = std::distance(m_points.begin(), min_el);
    m_max_y_index_zoom = std::distance(m_points.begin(), max_el);
}

void Viewport::zoomOut(double zoom_center)
{
    if (m_points.size() == 0)
        return;
    zoom_center = std::clamp(zoom_center, m_min_x_zoom, m_max_x_zoom);
    auto distance_left { (zoom_center - m_min_x_zoom) *zoom_factor };
    auto distance_right { (m_max_x_zoom - zoom_center) *zoom_factor };
    auto bound_min { m_points.begin()->first };
    auto bound_max { m_points.rbegin()->first };
    m_min_x_zoom = std::clamp(zoom_center - distance_left, bound_min, bound_max);
    m_max_x_zoom = std::clamp(zoom_center + distance_right, bound_min, bound_max);
    updateYMinMax();
}

void Viewport::restoreFullScale()
{
    if (m_points.size() == 0)
        return;
    m_min_y_index_zoom = m_min_y_index_total;
    m_max_y_index_zoom = m_max_y_index_total;
    m_min_x_zoom = m_points.begin()->first;
    m_max_x_zoom = m_points.rbegin()->first;
}

void Viewport::moveLeft()
{
    if (m_points.size() < 2)
        return;
    auto full_distance { m_max_x_zoom - m_min_x_zoom };
    auto move_distance { full_distance / 5.0 };
    auto new_left_position { m_min_x_zoom - move_distance };
    auto min_x_total { m_points.begin()->first };
    if (new_left_position < min_x_total) {
        new_left_position = min_x_total;
        move_distance = m_min_x_zoom - new_left_position;
    }
    auto new_right_position { m_max_x_zoom - move_distance };
    m_min_x_zoom = new_left_position;
    m_max_x_zoom = new_right_position;
    updateYMinMax();
}

void Viewport::moveRight()
{
    if (m_points.size() < 2)
        return;
    auto full_distance { m_max_x_zoom - m_min_x_zoom };
    auto move_distance { full_distance / 5.0 };
    auto new_right_position { m_max_x_zoom + move_distance };
    auto max_x_total { m_points.rbegin()->first };
    if (new_right_position > max_x_total) {
        new_right_position = max_x_total;
        move_distance = new_right_position - m_max_x_zoom;
    }
    auto new_left_position { m_min_x_zoom + move_distance };
    m_min_x_zoom = new_left_position;
    m_max_x_zoom = new_right_position;
    updateYMinMax();

}

std::pair<Point, Point> Viewport::visibleRect()
{
    if (m_points.size() != 0) {
        auto left_index { leftPointIndex() };
        auto right_index { rightPointIndex() };
        return { { m_points.at(left_index).first, m_points.at(m_min_y_index_zoom).second },
            { m_points.at(right_index).first, m_points.at(m_max_y_index_zoom).second } };
    } else {
        return { };
    }
}

void Viewport::resetAll()
{
    if (m_points.size() > 0) {
        auto [min_el, max_el] { minmaxElementByY(m_points.begin(), m_points.end()) };
        m_min_y_index_total = std::distance(m_points.begin(), min_el);
        m_max_y_index_total = std::distance(m_points.begin(), max_el);
        restoreFullScale();
    } else {
        m_min_y_index_total = m_max_y_index_total =
                                  m_min_y_index_zoom = m_max_y_index_zoom = 0u;
        m_min_x_zoom = m_max_x_zoom = 0.0;
    }
}

size_t Viewport::leftPointIndex() const
{
    if (m_points.size() == 0)
        throw std::logic_error("No left index for empty point set");
    if (m_min_x_zoom == m_points.begin()->first)
        return 0;
    auto lower_bound { std::lower_bound(m_points.begin(), m_points.end(), std::pair{m_min_x_zoom, 0.0},
    [] (auto a, auto b) { return a.first < b.first; } ) };
    auto index { static_cast<size_t>(std::distance(m_points.begin(), lower_bound)) };
    if (index == m_points.size())
        index--;
    // Include 1 more points out of scope if possible
    if (index > 0)
        index--;
    return index;
}

size_t Viewport::rightPointIndex() const
{
    if (m_points.size() == 0)
        throw std::logic_error("No right index for empty point set");
    if (m_max_x_zoom == m_points.rbegin()->first)
        return m_points.size() - 1;
    auto upper_bound { std::upper_bound(m_points.begin(), m_points.end(), std::pair{m_max_x_zoom, 0.0},
    [] (auto a, auto b) { return a.first < b.first; } ) };
    auto index { static_cast<size_t>(std::distance(m_points.begin(), upper_bound)) };
    if (index == m_points.size())
        index--;
    // 1 more points out of scope is already included...
    return index;
}

Viewport::PixelIterator::PixelIterator(Viewport &viewport) : viewport(viewport)
{

}

ScreenPoint Viewport::PixelIterator::operator*()
{
    if (point_index == 0)
        last_screen_point = convertPointToPixelPos();
    return last_screen_point;
}

Viewport::PixelIterator &Viewport::PixelIterator::operator++()
{
    ScreenPoint new_point { };
    do {
        if (++point_index >= viewport.m_points.size())
            return *this;
        new_point = convertPointToPixelPos();
    } while (new_point == last_screen_point);
    last_screen_point = new_point;
    return *this;
}

bool Viewport::PixelIterator::operator!=(const Viewport::PixelIterator::Sentinel &) const
{
    return point_index < viewport.m_points.size();
}

ScreenPoint Viewport::PixelIterator::convertPointToPixelPos()
{
    auto point { viewport.m_points.at(point_index) };
    auto x_min { viewport.m_min_x_zoom };
    auto x_max { viewport.m_max_x_zoom };
    auto y_min { viewport.m_points.at(viewport.m_min_y_index_zoom).second };
    auto y_max { viewport.m_points.at(viewport.m_max_y_index_zoom).second };
    auto x { double { } };
    auto y { double { } };

    if (x_min != x_max)
        x = (point.first - x_min) / (x_max - x_min) * viewport.m_pixel_width;
    else
        x = viewport.m_pixel_width / 2;

    if (y_min != y_max)
        y = viewport.m_pixel_height - ((point.second - y_min) / (y_max - y_min) * viewport.m_pixel_height);
    else
        y = viewport.m_pixel_height / 2;

    return { x, y };
}

}
