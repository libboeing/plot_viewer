/**
    Plot Viewer project
    pointsetreader.cpp
*/

#include <algorithm>
#include <cctype>
#include <cmath>
#include <filesystem>
#include <fstream>
#include <iterator>
#include <string>
#include <string_view>
#include <algorithm/string.hpp>
#include <lexical_cast.hpp>
#include <locale.hpp>
#include "plotexception.h"
#include "pointsetreader.h"

using namespace std::literals;

static const auto msg_end_reached { "End of stream is already reached"sv };

namespace PlotModel {

PointsetReader::PointsetReader(const std::filesystem::path &filename)
{
    m_stream = std::ifstream(filename, std::ios_base::in);

    // First, read header
    auto line { std::string { } };
    while (std::getline(m_stream, line)) {
        boost::trim(line);
        // Check if we are still in header
        if (line.size() > 0 && line.at(0) == '#') {
            auto shrinked_line { std::string{} };
            // remove consecutive whitespaces
            std::unique_copy(line.begin(), line.end(), std::back_insert_iterator(shrinked_line),
            [](auto a, auto b) { return std::iswspace(a) && iswspace(b);});
            // replace other whitespaces with spaces
            for (auto &c : shrinked_line)
                if (std::iswspace(c))
                    c = ' ';
            // input is in CP1251, convert!
            auto converted_str { boost::locale::conv::between(shrinked_line, "UTF-8", "CP1251") + "\n" };
            m_header.append(converted_str);
        } else {
            m_current_line = line;
            break;
        }
    }

    // First point is parsed right away
    if (m_current_line.empty()) {
        finishParsing();
        return;
    } else {
        try {
            parseCurrentPoint();
        } catch (...) {
            finishParsing();
            throw;
        }
    }

    // Next, estimate how many points we have (counting line feeds)
    {
        const auto old_position { m_stream.tellg() };
        auto iter { std::istreambuf_iterator<char>(m_stream) };
        const auto newlines_count { std::count_if(iter, std::istreambuf_iterator<char> { },
        [this] (auto c) { return c == '\n'; } ) };
        // Add first line, also last line might miss \n
        m_estimated_size = newlines_count + 2;
        m_stream.seekg(old_position);
    }
}

PointsetReader::PointsetReader(PointsetReader &&rhs) :
    m_estimated_size (rhs.m_estimated_size),
    m_end_reached (rhs.m_end_reached),
    m_stream (std::move(rhs.m_stream)),
    m_current_line (std::move(rhs.m_current_line)),
    m_current_point (rhs.m_current_point),
    m_header (std::move(rhs.m_header))
{ }

PointsetReader &PointsetReader::operator=(PointsetReader &&rhs)
{
    m_estimated_size = rhs.m_estimated_size;
    m_end_reached = rhs.m_end_reached;
    m_stream = std::move(rhs.m_stream);
    m_current_line = std::move(rhs.m_current_line);
    m_current_point = rhs.m_current_point;
    m_header = std::move(rhs.m_header);
    return *this;
}

PointsetReader::Iterator::Iterator(PointsetReader &reader) :
    m_parent(reader)
{

}

Point PointsetReader::Iterator::operator*() const
{
    if (!m_parent.m_end_reached)
        return m_parent.m_current_point;
    else
        throw PlotException(msg_end_reached);
}

PointsetReader::Iterator &PointsetReader::Iterator::operator++()
{
    if (m_parent.m_end_reached)
        throw PlotException(msg_end_reached);

    try {
        if (std::getline(m_parent.m_stream, m_parent.m_current_line)) {
            boost::trim(m_parent.m_current_line);
            // Sometimes input file contains phantom empty lines,
            // proceed to next line then
            if (m_parent.m_current_line.size() == 0)
                return operator++();
            m_parent.parseCurrentPoint();
        } else {
            m_parent.finishParsing();
        }
    } catch (const PlotException &) {
        m_parent.finishParsing();
        throw;
    }
    return *this;
}

bool PointsetReader::Iterator::operator!=(const Sentinel &) const
{
    return !m_parent.m_end_reached;
}

void PointsetReader::parseCurrentPoint()
{
    try {
        auto words { std::vector<std::string> { } };
        // Remove consecutibe whitespaces
        auto shrinked_line { std::string{} };
        std::unique_copy(m_current_line.begin(), m_current_line.end(),
                         std::back_insert_iterator(shrinked_line),
        [](auto a, auto b) { return std::iswspace(a) && std::iswspace(b);});
        boost::split(words, shrinked_line, [] (char c) { return std::isspace(c); });
        if (words.size() != 2)
            throw PlotException("Error parsing: wrong number of words");
        auto old_position { m_current_point.first };
        m_current_point = {
            boost::lexical_cast<double>(words.at(0)),
            boost::lexical_cast<double>(words.at(1)),
        };
        // Check for valid values
        for (const auto v : {m_current_point.first, m_current_point.second}) {
            if (std::isnan(v) || std::isinf(v))
                throw PlotException("Error parsing: undefined value");
        };
        if (m_current_point.first < old_position) // if equal, accept: timeline is still sorted
            throw PlotException("Error parsing: unsorted positions");
    } catch (const boost::bad_lexical_cast &) {
        throw PlotException("Error parsing: wrong number format");
    } catch (const PlotException &) {
        throw;
    } catch (...) {
        throw PlotException("Error while parsing input file");
    }
}

void PointsetReader::finishParsing()
{
    m_end_reached = true;
    m_stream.close();
}

}
