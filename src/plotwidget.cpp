/**
    Plot Viewer project
    plotwidget.cpp
*/

#include <mutex>
#include <QBrush>
#include <QPainter>
#include <QPaintEvent>
#include <QPen>
#include <QRect>
#include <QSize>
#include "plotwidget.h"
#include "plotwidgetworker.h"

PlotWidget::PlotWidget(QWidget *parent) :
    QWidget(parent),
    m_worker(new PlotWidgetWorker(*this))
{
    m_worker->moveToThread(m_worker);
    m_worker->start();
    connect(this, SIGNAL(requestedLoadPoints(QString)), m_worker, SLOT(slotLoadPoints(QString)));
    connect(this, SIGNAL(requestedUpdatePlotRect(QRect)), m_worker, SLOT(slotUpdatePlotRect(QRect)));
    connect(this, SIGNAL(requestedUpdatePlotRectOnly(QRect)), m_worker,
            SLOT(slotUpdatePlotRectOnly(QRect)));
    connect(this, SIGNAL(requestedGeneratePlotRender()), m_worker, SLOT(slotGeneratePlotRender()));
    connect(m_worker, SIGNAL(busyChanged(bool)), this, SIGNAL(busyChanged(bool)));
    connect(m_worker, SIGNAL(errorOccurred(QString)), this, SIGNAL(errorOccurred(QString)));
    connect(m_worker, SIGNAL(generatedPlotRender()), this, SLOT(slotDrawPlot()));

    connect(this, SIGNAL(requstedZoomIn()), m_worker, SLOT(slotZoomIn()));
    connect(this, SIGNAL(requstedZoomOut()), m_worker, SLOT(slotZoomOut()));
    connect(this, SIGNAL(requstedUnzoom()), m_worker, SLOT(slotUnzoom()));
    connect(this, SIGNAL(requstedMoveLeft()), m_worker, SLOT(slotMoveLeft()));
    connect(this, SIGNAL(requstedMoveRight()), m_worker, SLOT(slotMoveRight()));

    connect(m_worker, SIGNAL(headerChanged(QString)), this, SIGNAL(headerChanged(QString)));
}

QSize PlotWidget::minimumSizeHint() const
{
    return { m_left_panel_width + 100, m_bottom_panel_height + 100 };
}

void PlotWidget::paintEvent(QPaintEvent *)
{
    auto painter { QPainter { this } };
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setPen(QPen {Qt::black, 1});
    painter.setBrush(QBrush {Qt::white});

    // Outer rectangle
    painter.drawRect(0, 0, width(), height());

    // Y axis
    painter.drawLine( QLine {
        m_left_panel_width - 1,
        m_margin,
        m_left_panel_width - 1,
        height() - m_bottom_panel_height + 5 } );
    // X axis
    painter.drawLine( QLine {
        m_left_panel_width - 1 - 5,
        height() - m_bottom_panel_height + 1,
        width() - m_margin,
        height() - m_bottom_panel_height + 1 } );

    // Request plot render or draw if available
    painter.save();
    if (!new_plot_render_available) {
        if (!m_worker->isGenerationActive())
            emit requestedGeneratePlotRender();
    } else {
        new_plot_render_available = false;
        painter.setClipping(true);
        auto rect { m_worker->plotRect() };
        painter.setClipRect(rect);
        painter.translate(rect.topLeft());
        {
            auto lock { std::unique_lock(m_worker->plot_pixmap_mutex, std::try_to_lock) };
            if (lock.owns_lock())
                painter.drawPixmap(0, 0, *(m_worker->plot_pixmap));
        }
        // Draw marker, maybe?
        if (marker_position >= 0 && marker_position < rect.width()) {
            painter.setPen(QPen {Qt::green, 1});
            painter.drawLine(QLine { marker_position, 0, marker_position, rect.height() - 1 });
        }
    }
    painter.restore();

    // Draw text (boundaries)
    {
        auto convert = [] (double value) {
            return QString::number(value, 'g', 8);
        };
        auto pr { m_worker->plotRect() };
        auto vr { m_worker->visibleRect() };

        // X minimum value
        auto text_rect = QRectF(m_left_panel_width + 4, height() - m_bottom_panel_height + 5,
                                pr.width() / 2, m_bottom_panel_height);
        painter.drawText(text_rect, Qt::AlignLeft, convert(vr.first.first));

        // X maximum value
        text_rect = QRectF(m_left_panel_width + pr.width() / 2, height() - m_bottom_panel_height + 5,
                           pr.width() / 2 - 4, m_bottom_panel_height);
        painter.drawText(text_rect, Qt::AlignRight, convert(vr.second.first));

        // Y maximum value
        text_rect = QRectF(4, 4, m_left_panel_width - 10, 40);
        painter.drawText(text_rect, Qt::AlignRight, convert(vr.second.second));

        // Y minimum value
        text_rect = QRectF(4, m_margin + pr.height() - 20, m_left_panel_width - 10, 40);
        painter.drawText(text_rect, Qt::AlignRight, convert(vr.first.second));
    }
}

void PlotWidget::resizeEvent(QResizeEvent *)
{
    auto rect { QRect { m_left_panel_width, m_margin,
                        width() - m_margin - m_left_panel_width,
                        height() - m_margin - m_bottom_panel_height } };
    if (m_worker->isGenerationActive())
        emit requestedUpdatePlotRectOnly(rect);
    else
        emit requestedUpdatePlotRect(rect);
}

void PlotWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        auto pos { event->pos() };
        auto plot_rect { m_worker->plotRect() };
        if (plot_rect.contains(pos)) {
            marker_position = pos.x() - plot_rect.x();
            m_worker->setMarkerPosition(marker_position);
            new_plot_render_available = true;
            update();
        }
    }
}

void PlotWidget::slotDrawPlot()
{
    update();
}

