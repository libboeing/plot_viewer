QT += core gui widgets

CONFIG += c++17

INCLUDEPATH += $$PWD/include $$PWD/include/model /usr/include/boost

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    src/main.cpp \
    src/mainwindow.cpp \
    src/model/pointsetreader.cpp \
    src/model/viewport.cpp \
    src/plotwidget.cpp \
    src/plotwidgetworker.cpp

HEADERS += \
    include/mainwindow.h \
    include/model/pointsetreader.h \
    include/model/plotexception.h \
    include/model/viewport.h \
    include/plotwidget.h \
    include/plotwidgetworker.h

FORMS += \
    mainwindow.ui

LIBS += -L/usr/lib64 -lboost_locale

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc
